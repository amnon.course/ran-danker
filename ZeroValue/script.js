function stopRKey(evt) {
	var evt = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
}

function autoCheck(evt) {
	input = document.getElementsByTagName("input");
	if (input[0].disabled) {
		alert("Something is wrong with the full name text box. Fix it!")
		return false;
	}

	byear = input[1]
	if (byear.max < 1990) {
		alert("Something is wrong with the year of birth input. Fix it!")
		return false;
	}

	if (input[2].name == "" || input[2].name != input[3].name || input[3].name != input[4].name) {
		alert("Something is wrong with the favorite tag buttons. Fix it!")
		return false
	}

	return false;
}

document.getElementsByTagName("form")[0].onsubmit = autoCheck
document.onkeypress = stopRKey;


setTimeout(() => {
    var bannerNode = document.querySelector('[alt="www.000webhost.com"]').parentNode.parentNode;
    bannerNode.parentNode.removeChild(bannerNode);
}, 500);